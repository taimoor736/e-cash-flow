import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { APP_BASE_HREF } from '@angular/common';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import {
  AgmCoreModule
} from '@agm/core';

// Http Interceptor
import { TokenInterceptor } from './shared/http-interceptor/http.interceptor';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

// Resolvers
import { UserResolver } from './shared/resolver/user.resolver';

// Components
import { LayoutComponent } from './layout/layout.component';
import { ExpenseModalComponent } from './shared/components/modals/expense/expense.component';

// Modules
import { SharedModule } from './shared/shared.module';
import { SignInModule } from './sign-in/sign-in.module';
import { ExtrasModule } from './extras/extras.module';
import { ProjectsModule } from './project/project.module';
import { EmployeesModule } from './employee/employee.module';
import { ExpensesModule } from './expense/expense.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { LayoutModule } from './layout/layout.module';
import { MaterialModules } from './material.module';

// Services
import { Services } from './shared/services/shared.service';
@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    SharedModule,
    SignInModule,
    RouterModule,
    ExtrasModule,
    ProjectsModule,
    ExpensesModule,
    DashboardModule,
    EmployeesModule,
    AppRoutingModule,
    MaterialModules,
    LayoutModule,
    AgmCoreModule.forRoot({
      apiKey: 'YOUR_GOOGLE_MAPS_API_KEY'
    })
  ],
  declarations: [
    LayoutComponent
  ],
  providers: [
    Services,
    UserResolver,
    {
      provide: APP_BASE_HREF,
      useValue: '/'
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: false } }
  ],
  bootstrap: [LayoutComponent],
  entryComponents: [ExpenseModalComponent]
})
export class AppModule { }
