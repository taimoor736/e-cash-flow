import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/filter';
import { Router, NavigationEnd } from '@angular/router';
import {startCase} from 'lodash';

@Component({
    selector: 'app-root',
    templateUrl: './layout.html',
    styleUrls: ['./layout.scss']
})
export class LayoutComponent implements OnInit {

    constructor(private router: Router) {
        router.events.subscribe((val) => {
            if(val instanceof NavigationEnd) {
                let path = val.urlAfterRedirects
                            .split('/')
                            .filter(i => !!i)
                            .map(i => startCase(i))
                            .join(' / ');
                document.title = `Budget Manager - ${path}`;
            }
        });
     }

    ngOnInit() {
        // Todo: It should be implemented according to Angular Concepts
        if (window.location.pathname === '/') {
            this.router.navigate(['dashboard'])
        }
    }
}