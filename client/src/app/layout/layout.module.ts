import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BrowserModule } from '@angular/platform-browser';
import { LayoutRoutingModule } from './layout.routing';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    LayoutRoutingModule,
    SharedModule,
  ],
  declarations: [
  ],
  providers: [],
  schemas: [NO_ERRORS_SCHEMA]
})
export class LayoutModule {
}
