import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { GuardService } from '../shared/services/auth.guard.service';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        // resolve: {
        //     user: UserResolver
        // },

    }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class LayoutRoutingModule { }