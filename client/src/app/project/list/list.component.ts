import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectService, NotificationService } from '../../shared/services/shared.service';

@Component({
  selector: 'app-list-projects',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListProjectsComponent implements OnInit {
  projects: any = [];

  constructor(
    private router: Router,
    private projectService: ProjectService,
    private notificationService: NotificationService
  ) { }

  ngOnInit() {
    this.listProjects();
  }

  listProjects() {
    this.projectService.listProjects().then((res: any) => {
      this.projects = res;
    })
  }

  deleteProject(id: any) {
    this.projectService.deleteProject(id).then((res: any) => {
      this.notificationService.showNotification({
        message: 'Project deleted successfully!'
      });
      this.listProjects();
    }).catch((err: any) => {
      this.notificationService.showNotification({
        message: err,
        type: 'danger'
      });
      this.listProjects();
    })
  }

}
