import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProjectService, NotificationService, EmployeeService } from '../../shared/services/shared.service';
import * as _ from 'lodash';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-edit-project',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditProjectsComponent implements OnInit {

  projectForm = new FormGroup({
    id: new FormControl(''),
    name: new FormControl('', Validators.required),
    amount: new FormControl('', Validators.required),
    startDate: new FormControl('', Validators.required),
    endDate: new FormControl('', Validators.required),
    notes: new FormControl(''),
  });

  employees: any = [];
  projectEmployees: any = [{
    employeeId: '',
    ratingStrategy: '',
    amount: ''
  }];
  projectEmployeesCount: any = 0;
  employeesRatingList: any = ['Hour', 'Week', 'Month', 'Project']
  isProjectCreated: any = false;
  createdProject: any = {};
  constructor(
    private router: Router,
    private projectService: ProjectService,
    private notificationService: NotificationService,
    private employeeService: EmployeeService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.listEmployees();
    this.route.params.subscribe((params) => {
      this.getProject(params.id);
    })
  }

  getProject(id: any) {
    this.projectService.getById(id).then((res: any) => {
      this.projectForm.patchValue(res);
      this.listProjectEmployees(res.id);
    })
  }

  updateProject() {
    this.projectService.updateById(this.projectForm.value.id, this.projectForm.value).then((res: any) => {
      this.notificationService.showNotification({
        message: 'Project updated successfully!'
      });
    }).catch((err: any) => {
      this.notificationService.showNotification({
        message: err,
        type: 'danger'
      });
    })
  }

  listEmployees() {
    this.employeeService.listEmployees().then((res: any) => {
      this.employees = res;
    })
  }

  listProjectEmployees(projectId: any) {
    this.projectService.listProjectEmployees(projectId).then((res: any) => {
      this.projectEmployees = res;
    })
  }


  addProjectEmployee(projectEmployee: any, shouldRedirect: any) {

    if (!this.checkProjectEmployeeObjectValid(projectEmployee))
      return;

    this.projectService.addEmployeeIntoProject(this.projectForm.value.id, projectEmployee).then((res: any) => {
      this.projectEmployeesCount = this.projectEmployees.length;

      if (shouldRedirect) {
        this.navigateToListProjects()
      }

      this.projectEmployees.push({
        employeeId: '',
        ratingStrategy: '',
        amount: '',
      })
    }).catch((err: any) => {
      this.notificationService.showNotification({
        message: err,
        type: 'danger'
      });
    })
  }

  save() {
    if (this.projectEmployees.length > this.projectEmployeesCount) {
      this.addProjectEmployee(this.projectEmployees[this.projectEmployees.length - 1], true);
    }
    this.navigateToListProjects()

  }

  removeProjectEmployee(index: any) {
    if (!this.checkProjectEmployeeObjectValid(this.projectEmployees[index])) {
      this.projectEmployees.splice(index, 1);
      return;
    }
    this.projectService.removeEmployeeIntoProject(this.projectForm.value.id, { employeeId: this.projectEmployees[index].employeeId }).then((res: any) => {
      this.projectEmployees.splice(index, 1);
    }).catch((err: any) => {
      this.notificationService.showNotification({
        message: err,
        type: 'danger'
      });
    })
  }

  navigateToListProjects() {
    this.router.navigate(['/list-projects'])
  }

  checkProjectEmployeeObjectValid(projectEmployee: any) {
    return _.keys(projectEmployee).length === _.compact(_.values(projectEmployee)).length
  }


}



