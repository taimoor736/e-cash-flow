import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectService, NotificationService, EmployeeService } from '../../shared/services/shared.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as _ from 'lodash';
@Component({
  selector: 'app-create-project',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateProjectsComponent implements OnInit {

  projectForm = new FormGroup({
    name: new FormControl('', Validators.required),
    amount: new FormControl('', Validators.required),
    startDate: new FormControl('', Validators.required),
    endDate: new FormControl('', Validators.required),
    notes: new FormControl(''),
  });
  employees: any = [];
  projectEmployees: any = [{
    employeeId: '',
    ratingStrategy: '',
    amount: ''
  }];
  projectEmployeesCount: any = 0;
  employeesRatingList: any = ['Hour', 'Week', 'Month', 'Project']
  isProjectCreated: any = false;
  createdProject: any = {};
  constructor(
    private router: Router,
    private projectService: ProjectService,
    private notificationService: NotificationService,
    private employeeService: EmployeeService
  ) { }

  ngOnInit() {
    this.listEmployees();
  }

  createProject() {
    this.projectService.createProject(this.projectForm.value).then((res: any) => {
      this.isProjectCreated = true;
      this.createdProject = res;
      this.notificationService.showNotification({
        message: 'Project created successfully!'
      });
    }).catch((err: any) => {
      this.notificationService.showNotification({
        message: err,
        type: 'danger'
      });
    })
  }

  listEmployees() {
    this.employeeService.listEmployees().then((res: any) => {
      this.employees = res;
    })
  }


  addProjectEmployee(projectEmployee: any, shouldRedirect: any) {

    if (!this.checkProjectEmployeeObjectValid(projectEmployee))
      return;

    this.projectService.addEmployeeIntoProject(this.createdProject.id, projectEmployee).then((res: any) => {
      this.projectEmployeesCount = this.projectEmployees.length;

      if (shouldRedirect) {
        this.navigateToListProjects()
      }

      this.projectEmployees.push({
        employeeId: '',
        ratingStrategy: '',
        amount: '',
      })
    }).catch((err: any) => {
      this.notificationService.showNotification({
        message: err,
        type: 'danger'
      });
    })
  }

  save() {
    if (this.projectEmployees.length > this.projectEmployeesCount) {
      this.addProjectEmployee(this.projectEmployees[this.projectEmployees.length - 1], true);
    }
    this.navigateToListProjects()

  }

  removeProjectEmployee(index: any) {
    if (!this.checkProjectEmployeeObjectValid(this.projectEmployees[index])) {
      this.projectEmployees.splice(index, 1);
      return;
    }
    this.projectService.removeEmployeeIntoProject(this.createdProject.id, { employeeId: this.projectEmployees[index].employeeId }).then((res: any) => {
      this.projectEmployees.splice(index, 1);
    }).catch((err: any) => {
      this.notificationService.showNotification({
        message: err,
        type: 'danger'
      });
    })
  }

  navigateToListProjects() {
    this.router.navigate(['/list-projects'])
  }

  checkProjectEmployeeObjectValid(projectEmployee: any) {
    return _.keys(projectEmployee).length === _.compact(_.values(projectEmployee)).length
  }
}
