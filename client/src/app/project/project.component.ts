import { Component } from '@angular/core';

import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'projects-app',
  templateUrl: './project.html',
  styleUrls: ['./project.scss']
})
export class ProjectsComponent {

  constructor(private route: ActivatedRoute, private router: Router) {

  }
  ngOnInit() {

  }

}