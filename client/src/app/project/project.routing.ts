import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ListProjectsComponent } from './list/list.component';
import { ProjectsComponent } from './project.component';
import { CreateProjectsComponent } from './create/create.component';
import { EditProjectsComponent } from './edit/edit.component';
import { GuardService } from '../shared/services/auth.guard.service';

const routes: Routes = [
  {
    path: 'project',
    component: ProjectsComponent,
    canActivate: [GuardService],
    resolve: {
      //   user: UserResolver
    },
    children: [
      {
        path: '',
        redirectTo: '/project/list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        component: ListProjectsComponent,
      },
      {
        path: 'create',
        component: CreateProjectsComponent,
      }, {
        path: 'edit/:id',
        component: EditProjectsComponent,
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class ProjectsRoutesModule { }
export const ProjectsComponents = [
  ListProjectsComponent,
  ProjectsComponent,
  CreateProjectsComponent,
  EditProjectsComponent
]