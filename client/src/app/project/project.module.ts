import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectsRoutesModule, ProjectsComponents } from './project.routing';

import { MaterialModules } from '../material.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ProjectsRoutesModule,
    MaterialModules,
    SharedModule
  ],
  declarations: [
    ProjectsComponents
  ],
  providers: [
  ]
})

export class ProjectsModule { }
