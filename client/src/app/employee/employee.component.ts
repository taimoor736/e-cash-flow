import { Component } from '@angular/core';

import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'employees-app',
  templateUrl: './employee.html',
  styleUrls: ['./employee.scss']
})
export class EmployeesComponent {

  constructor(private route: ActivatedRoute, private router: Router) {

  }
  ngOnInit() {

  }

}