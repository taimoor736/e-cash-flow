import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { EmployeeService, NotificationService } from '../../shared/services/shared.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employee-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class EmployeeProfileComponent implements OnInit {

  employeeForm = new FormGroup({
    id: new FormControl(''),
    firstName: new FormControl('', Validators.required),
    middleName: new FormControl(''),
    lastName: new FormControl('', Validators.required),
    addresses: new FormControl(''),
    email: new FormControl('', [Validators.required, Validators.email]),
  });

  constructor(
    private employeeService: EmployeeService,
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.getEmployee(params.id);
    });
  }

  getEmployee(id: any) {
    this.employeeService.getById(id).then((res: any) => {
      this.employeeForm.patchValue(res);
    })
  }

  updateEmployee() {
    this.employeeService.updateById(this.employeeForm.value.id, this.employeeForm.value).then((res: any) => {
      this.notificationService.showNotification({
        message: 'Employee updated successfully!'
      });
      this.router.navigate(['/list-employees']);
    }).catch((err: any) => {
      this.notificationService.showNotification({
        message: err,
        type: 'danger'
      });
    })
  }

}
