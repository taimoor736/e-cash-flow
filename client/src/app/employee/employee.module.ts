import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeesRoutesModule, EmployeesComponents } from './employee.routing';
import { MaterialModules } from '../material.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    EmployeesRoutesModule,
    MaterialModules,
    SharedModule
  ],
  declarations: [
    EmployeesComponents,
  ],
  providers: [
  ]
})

export class EmployeesModule { }
