import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ListEmployeesComponent } from './list/list.component';
import { EmployeeProfileComponent } from './profile/profile.component';
import { CreateEmployeeComponent } from './create/create.component';
import { EmployeesComponent } from './employee.component';
import { GuardService } from '../shared/services/auth.guard.service';

const routes: Routes = [
  {
    path: 'employee',
    component: EmployeesComponent,
    canActivate: [GuardService],
    resolve: {
      //   user: UserResolver
    },
    children: [
      {
        path: '',
        redirectTo: '/employee/list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        component: ListEmployeesComponent,
      },
      {
        path: 'profile/:id',
        component: EmployeeProfileComponent,
      },
      {
        path: 'create',
        component: CreateEmployeeComponent,
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class EmployeesRoutesModule { }
export const EmployeesComponents = [
  ListEmployeesComponent,
  EmployeeProfileComponent,
  EmployeesComponent,
  CreateEmployeeComponent,
]
