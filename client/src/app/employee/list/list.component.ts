import { Component, OnInit, Inject } from '@angular/core';
import { EmployeeService, NotificationService } from '../../shared/services/shared.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-employees-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListEmployeesComponent implements OnInit {
  employees: any = [];
  constructor(
    private employeeService: EmployeeService,
    private router: Router,
    private notificationService: NotificationService
  ) { }

  ngOnInit() {
    this.listEmployees();
  }

  listEmployees() {
    this.employeeService.listEmployees().then((res: any) => {
      this.employees = res;
    })
  }

  deleteEmployee(id: any) {
    this.employeeService.deleteEmployee(id).then((res: any) => {
      this.notificationService.showNotification({
        message: 'Employee deleted successfully!'
      });
      this.listEmployees();
    }).catch((err: any) => {
      this.notificationService.showNotification({
        message: err,
        type: 'danger'
      });
      this.listEmployees();
    })
  }

}
