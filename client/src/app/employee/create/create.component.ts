import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { EmployeeService, NotificationService } from '../../shared/services/shared.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateEmployeeComponent implements OnInit {

  employeeForm = new FormGroup({
    id: new FormControl(''),
    firstName: new FormControl('', Validators.required),
    middleName: new FormControl(''),
    lastName: new FormControl('', Validators.required),
    addresses: new FormControl(''),
    email: new FormControl('', [Validators.required, Validators.email]),
  });

  constructor(
    private employeeService: EmployeeService,
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService
  ) { }

  ngOnInit() {

  }

  createEmployee() {
    this.employeeService.createEmployee(this.employeeForm.value).then((res: any) => {
      this.notificationService.showNotification({
        message: 'Employee created successfully!'
      });
      this.router.navigate(['/list-employees'])
    })
  }

}
