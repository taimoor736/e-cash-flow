import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FooterComponent } from './components/footer/footer.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { ExpenseModalComponent } from './components/modals/expense/expense.component';
import { MaterialModules } from '../material.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialModules
  ],
  declarations: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    ExpenseModalComponent
  ],
  exports: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    ExpenseModalComponent
  ]
})
export class SharedModule { }
