import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard', icon: 'dashboard', class: '' },
    { path: '/employee', title: 'Employees', icon: 'assignment_ind', class: '' },
    { path: '/project', title: 'Projects', icon: 'assignment', class: '' },
    { path: '/expense', title: "Expenses", icon: 'money', class: '' },
    // { path: '/list-expense-items', title: "Expense's Items", icon: 'content_paste', class: '' },
    // { path: '/employee-profile/2', title: "Employee Profile", icon: 'content_paste', class: '' },
];

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    menuItems: any[];

    constructor(private userService: UserService) { }

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }
    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
}
