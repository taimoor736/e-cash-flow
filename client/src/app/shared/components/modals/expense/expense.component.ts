import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-expense',
  templateUrl: './expense.component.html',
  styleUrls: ['./expense.component.scss']
})
export class ExpenseModalComponent {

  expenseItemForm = new FormGroup({
    id: new FormControl(''),
    expenseId: new FormControl(''),
    name: new FormControl('', Validators.required),
  });
  modal: any = {};
  constructor(
    public dialogRef: MatDialogRef<ExpenseModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any = {}) { }

  ngOnInit() {
    this.expenseItemForm.patchValue(this.data.expenseItem);
    this.modal.onNoClick = this.onNoClick.bind(this);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
