import { Injectable } from '@angular/core';
const apiVersion = '/api/v1/';

import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { CookieService } from '../services/cookie.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public auth: CookieService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    req = req.clone({ headers: req.headers.set('Authorization', `Bearer ${this.auth.getCookie('token')}`) });
    req = req.clone({ url: ~req.url.indexOf('/api') ? req.url : apiVersion + req.url });
    req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
    return next.handle(req);
  }
}
