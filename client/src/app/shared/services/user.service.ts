import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CookieService } from "./cookie.service";
import { Router } from '@angular/router';

@Injectable()
export class UserService {
    user: any = {};

    constructor(private http: HttpClient, private cookieService: CookieService, private router: Router) { }

    getMe(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (Object.keys(this.user).length) {
                resolve({ user: this.user })
            } else {
                this.http.get('/api/me').subscribe((res: any) => {
                    Object.assign(this.user, res.user)
                    resolve(res);
                }, (error: any) => {
                    reject(error.error);
                });
            }
        });
    }

    login(user: any) {
        return new Promise((resolve, reject) => {
            this.http.post('/api/login', user).subscribe((res: any) => {
                this.cookieService.setCookie('token', res.token, 15);
                Object.assign(this.user, res.user)
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        });
    }

    logout() {
        this.user = {};
        this.cookieService.removeCookie();  
        this.router.navigate(['/sign-in'])
    }
}
