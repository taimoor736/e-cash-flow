import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
const projectUrl: any = 'admin/project';
@Injectable()
export class ProjectService {

    constructor(private http: HttpClient, private router: Router) { }

    listProjects() {
        return new Promise((resolve, reject) => {
            this.http.get(projectUrl).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        });
    }

    getById(id: any) {
        return new Promise((resolve, reject) => {
            this.http.get(`${projectUrl}/${id}`).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        })
    }

    updateById(id: any, project: any) {
        return new Promise((resolve, reject) => {
            this.http.put(`${projectUrl}/${id}`, project).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        })
    }

    deleteProject(id: any) {
        return new Promise((resolve, reject) => {
            this.http.delete(`${projectUrl}/${id}`).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        })
    }

    createProject(project: any) {
        return new Promise((resolve, reject) => {
            this.http.post(projectUrl, project).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        })
    }

    addEmployeeIntoProject(projectId: any, employees) {
        return new Promise((resolve, reject) => {
            this.http.post(`${projectUrl}/${projectId}/add-employee`, employees).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        })
    }

    removeEmployeeIntoProject(projectId: any, employee: any) {
        return new Promise((resolve, reject) => {
            this.http.put(`${projectUrl}/${projectId}/remove-employee`, employee).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        })
    }

    listProjectEmployees(projectId: any) {
        return new Promise((resolve, reject) => {
            this.http.get(`${projectUrl}/${projectId}/employees`).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        });
    }

}
