import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
const employeeUrl: any = 'admin/employee';
@Injectable()
export class EmployeeService {

    constructor(private http: HttpClient, private router: Router) { }

    listEmployees() {
        return new Promise((resolve, reject) => {
            this.http.get(employeeUrl).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        });
    }

    getById(id: any) {
        return new Promise((resolve, reject) => {
            this.http.get(`${employeeUrl}/${id}`).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        })
    }

    updateById(id: any, employee: any) {
        return new Promise((resolve, reject) => {
            this.http.put(`${employeeUrl}/${id}`, employee).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        })
    }

    createEmployee(employee: any) {
        return new Promise((resolve, reject) => {
            this.http.post(employeeUrl, employee).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        })
    }

    deleteEmployee(id: any) {
        return new Promise((resolve, reject) => {
            this.http.delete(`${employeeUrl}/${id}`).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        })
    }

}
