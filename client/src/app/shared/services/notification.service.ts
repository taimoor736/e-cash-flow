import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
declare var $: any;


@Injectable()
export class NotificationService {

    constructor(private http: HttpClient) { }

    showNotification({
        message = 'Task performed successfully!',
        type = 'success'
    }) {
        const warningOrDanger = ['', 'warning', 'danger'];
        const successOrInfo = ['info', 'success']
        let placement = {};

        if (~successOrInfo.indexOf(type)) {
            placement = {
                from: 'top',
                align: 'right'
            }
        } else if (~warningOrDanger.indexOf(type)) {
            placement = {
                from: 'bottom',
                align: 'right'
            }
        }

        $.notify({
            icon: "notifications",
            message: message

        }, {
                type: type,
                timer: 4000,
                placement: placement,
                template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
                    '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
                    '<i class="material-icons" data-notify="icon">notifications</i> ' +
                    '<span data-notify="title">{1}</span> ' +
                    '<span data-notify="message">{2}</span>' +
                    '<div class="progress" data-notify="progressbar">' +
                    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                    '</div>' +
                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                    '</div>'
            });
    }


}
