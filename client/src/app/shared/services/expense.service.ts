import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
const expenseUrl: any = 'admin/expense';
@Injectable()
export class ExpenseService {

    constructor(private http: HttpClient, private router: Router) { }

    listExpenses() {
        return new Promise((resolve, reject) => {
            this.http.get(expenseUrl).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        });
    }

    getById(id: any) {
        return new Promise((resolve, reject) => {
            this.http.get(`${expenseUrl}/${id}`).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        })
    }

    updateById(id: any, expense: any) {
        return new Promise((resolve, reject) => {
            this.http.put(`${expenseUrl}/${id}`, expense).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        })
    }

    deleteExpense(id: any) {
        return new Promise((resolve, reject) => {
            this.http.delete(`${expenseUrl}/${id}`).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        })
    }

    deleteExpenseItem(expenseId: any, expenseItemId: any) {
        return new Promise((resolve, reject) => {
            this.http.delete(`${expenseUrl}/${expenseId}/item/${expenseItemId}`).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        })
    }

    updateExpenseItemById(expenseId: any, expenseItemId: any, expenseItem: any) {
        return new Promise((resolve, reject) => {
            this.http.put(`${expenseUrl}/${expenseId}/item/${expenseItemId}`, expenseItem).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        })
    }

    createExpense(expense: any) {
        return new Promise((resolve, reject) => {
            this.http.post(expenseUrl, expense).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        })
    }


    createExpenseItem(expenseId: any, expenseItem: any) {
        return new Promise((resolve, reject) => {
            this.http.post(`${expenseUrl}/${expenseId}/item`, expenseItem).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        })
    }

    listExpenseItemsByExpenseId(id: any) {
        return new Promise((resolve, reject) => {
            this.http.get(`${expenseUrl}/${id}/item`).subscribe((res: any) => {
                resolve(res);
            }, (error: any) => {
                reject(error.error);
            });
        })
    }

}
