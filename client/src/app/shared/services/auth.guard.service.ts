
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CookieService } from '../services/cookie.service';
import { UserService } from '../services/user.service';
@Injectable()
export class GuardService implements CanActivate {

    constructor(private router: Router, private cookieService: CookieService, private userService: UserService) { }

    canActivate(): boolean | Observable<boolean> | Promise<boolean> {
        if (!this.cookieService.getCookie('token')) {
            this.router.navigate(['/sign-in']);
            return false;
        }
        return new Promise((resolve, reject) => {
            this.userService.getMe().then((res: any) => {
                if (!res.user) {
                    this.router.navigate(['/sign-in']);
                    resolve(false);
                    return;
                }
                resolve(true);
            }).catch((err) => {
                this.router.navigate(['/sign-in']);
                resolve(false);
            });
        });
    }
}
