import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { TypographyComponent } from './typography/typography.component';
import { UpgradeComponent } from './upgrade/upgrade.component';
import { TableListComponent } from './table-list/table-list.component';
import { ExtrasComponent } from './extras.component';

const routes: Routes = [
  {
    path: '',
    component: ExtrasComponent,
    // canActivate: [GuardService],
    resolve: {
    //   user: UserResolver
    },
    children: [
      {
        path: 'icons',
        component: IconsComponent,
      },
      {
        path: 'maps',
        component: MapsComponent,
      },
      {
        path: 'notifications',
        component: NotificationsComponent,
      },
      {
        path: 'typography',
        component: TypographyComponent,
      },
      {
        path: 'upgrade',
        component: UpgradeComponent,
      },
      {
        path: 'table-list',
        component: TableListComponent,
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class ExtrasRoutesModule {}
export const ExtrasComponents = [
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    TypographyComponent,
    UpgradeComponent,
    TableListComponent,
    ExtrasComponent
]