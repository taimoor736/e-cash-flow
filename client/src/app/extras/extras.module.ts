import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExtrasRoutesModule, ExtrasComponents } from './extras.routing';
import { SharedModule } from '../shared/shared.module';

import { MaterialModules } from '../material.module';
@NgModule({
  imports: [
    CommonModule,
    ExtrasRoutesModule,
    MaterialModules,
    SharedModule
  ],
  declarations: [
    ExtrasComponents
  ],
  providers: [
  ]
})

export class ExtrasModule { }
