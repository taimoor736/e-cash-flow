import { Component, OnInit } from '@angular/core';
import { UserService, CookieService, NotificationService } from '../shared/services/shared.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  templateUrl: './sign-in.html',
  styleUrls: ['./sign-in.scss']
})
export class SignIn implements OnInit {

  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
  });

  constructor(
    private userService: UserService,
    private router: Router,
    private cookieService: CookieService,
    private notificationService: NotificationService
  ) { }

  ngOnInit() {
    if (this.cookieService.getCookie('token')) {
      this.router.navigate(['/dashboard']);
    }
  }

  login() {
    this.userService.login({
      email: this.loginForm.value.email,
      password: this.loginForm.value.password
    }).then((res: any) => {
      this.router.navigate(['/dashboard']);
    }).catch((err: any) => {
      this.notificationService.showNotification({
        message: err,
        type: 'danger'
      })
    })
  }

}
