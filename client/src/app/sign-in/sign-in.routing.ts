import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SignIn } from './sign-in.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';


const signInRoutes: Routes = [
  {
    path: 'sign-in',
    component: SignIn
  },
  {
    path: 'reset-password',
    component: ForgotPasswordComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(signInRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class signInRoutesModule { }
