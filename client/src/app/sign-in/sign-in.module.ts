import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignIn } from './sign-in.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { signInRoutesModule } from './sign-in.routing';
import { MaterialModules } from '../material.module';


@NgModule({
  imports: [
    CommonModule,
    signInRoutesModule,
    MaterialModules
  ],
  declarations: [
    SignIn,
    ForgotPasswordComponent
  ],
  providers: [
  ]
})
export class SignInModule {
}
