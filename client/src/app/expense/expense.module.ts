import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExpensesRoutesModule, ExpensesComponents } from './expense.routing';
import { MaterialModules } from '../material.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ExpensesRoutesModule,
    MaterialModules,
    SharedModule
  ],
  declarations: [
    ExpensesComponents
  ],
  providers: [
  ]
})

export class ExpensesModule { }
