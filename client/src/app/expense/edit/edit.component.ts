import { Component, OnInit } from '@angular/core';
import { ExpenseService, NotificationService } from '../../shared/services/shared.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';


@Component({
  selector: 'app-edit-expense',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditExpensesComponent implements OnInit {

  constructor(
    private expenseService: ExpenseService,
    private route: ActivatedRoute,
    private router: Router,
    private notificationService: NotificationService
  ) { }

  expenseForm = new FormGroup({
    id: new FormControl(''),
    name: new FormControl('', Validators.required),
  });
  expenseItems: any = [];
  ngOnInit() {
    this.route.params.subscribe((params: any) => {
      this.getExpense(params.id);
    })
  }

  getExpense(id: any) {
    this.expenseService.getById(id).then((res: any) => {
      this.expenseForm.patchValue(res);
      this.listExpenseItemsByExpenseId(res.id);
    });
  }

  listExpenseItemsByExpenseId(id: any) {
    this.expenseService.listExpenseItemsByExpenseId(id).then((res: any) => {

      if (!res.length) {
        this.addExpenseItem(1);
        return;
      }
      this.expenseItems = res;
    })
  }

  addExpenseItem(itemName: any) {
    if (!itemName)
      return;
    this.expenseItems.push({
      name: ''
    })
  }

  removeExpenseItem(index: any) {
    this.expenseItems.splice(index, 1);
  }

  updateExpense() {
    let expense = this.expenseForm.value;
    expense.expenseItems = this.expenseItems;
    this.expenseService.updateById(this.expenseForm.value.id, expense).then((res: any) => {
      this.notificationService.showNotification({
        message: 'Expense updated successfully!'
      });
      this.router.navigate(['/list-expenses']);
    }).catch((err: any) => {
      this.notificationService.showNotification({
        message: err,
        type: 'danger'
      });
    })
  }

}
