import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ListExpensesComponent } from './list/list.component';
import { ExpensesComponent } from './expense.component';
import { CreateExpensesComponent } from './create/create.component';
import { EditExpensesComponent } from './edit/edit.component';
import { GuardService } from '../shared/services/auth.guard.service';


const routes: Routes = [
  {
    path: 'expense',
    component: ExpensesComponent,
    canActivate: [GuardService],
    resolve: {
      //   user: UserResolver
    },
    children: [
      {
        path: '',
        redirectTo: '/expense/list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        component: ListExpensesComponent,
      },
      {
        path: 'create',
        component: CreateExpensesComponent,
      }, {
        path: 'edit/:id',
        component: EditExpensesComponent,
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class ExpensesRoutesModule { }
export const ExpensesComponents = [
  ListExpensesComponent,
  ExpensesComponent,
  CreateExpensesComponent,
  EditExpensesComponent
]