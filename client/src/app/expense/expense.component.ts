import { Component } from '@angular/core';

import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'expenses-app',
  templateUrl: './expense.html',
  styleUrls: ['./expense.scss']
})
export class ExpensesComponent {

  constructor(private route: ActivatedRoute, private router: Router) {

  }
  ngOnInit() {

  }

}