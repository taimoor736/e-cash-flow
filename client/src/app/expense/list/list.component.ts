import { Component, OnInit } from '@angular/core';
import { ExpenseService, NotificationService } from '../../shared/services/shared.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ExpenseModalComponent } from '../../shared/components/modals/expense/expense.component';
@Component({
  selector: 'app-list-expenses',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListExpensesComponent implements OnInit {

  constructor(
    private expenseService: ExpenseService,
    private router: Router,
    private notificationService: NotificationService,
    public dialog: MatDialog
  ) { }
  expenses: any = [];
  expenseItems: any = [];
  selectedExpense: any = {};

  ngOnInit() {
    this.listExpenses();
  }

  listExpenses() {
    this.expenseService.listExpenses().then((res: any) => {
      this.expenses = res;
      if (res.length) {
        this.listExpenseItemsByExpenseId(res[0]);
      }
    })
  }

  listExpenseItemsByExpenseId(expense: any) {
    this.selectedExpense = expense;
    this.expenseService.listExpenseItemsByExpenseId(expense.id).then((res: any) => {
      this.expenseItems = res;
    })
  }

  deleteExpense(id: any) {
    this.expenseService.deleteExpense(id).then((res: any) => {
      this.notificationService.showNotification({
        message: 'Expense deleted successfully!'
      });
      this.listExpenses();
    }).catch((err: any) => {
      this.notificationService.showNotification({
        message: err,
        type: 'danger'
      });
      this.listExpenses();
    })
  }

  deleteExpenseItem(expenseId: any, expenseItemId: any) {
    this.expenseService.deleteExpenseItem(expenseId, expenseItemId).then((res: any) => {
      this.notificationService.showNotification({
        message: 'Expense item deleted successfully!'
      });
      this.listExpenses();
    }).catch((err: any) => {
      this.notificationService.showNotification({
        message: err,
        type: 'danger'
      });
      this.listExpenses();
    })
  }

  openDialogForUpdation(expenseItem: any): void {
    const dialogRef = this.dialog.open(ExpenseModalComponent, {
      width: '550px',
      data: {
        expenseItem,
        isUpdateAble: true,
        callback: this.updateExpenseItem.bind(this)
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openDialogForCreation(expenseItem: any): void {
    const dialogRef = this.dialog.open(ExpenseModalComponent, {
      width: '550px',
      data: {
        expenseItem: {
          expenseId: expenseItem.id
        },
        isUpdateAble: false,
        callback: this.createExpenseItem.bind(this)
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  updateExpenseItem(data: any, modal: any) {
    this.expenseService.updateExpenseItemById(data.expenseId, data.id, { name: data.name }).then((res: any) => {
      this.notificationService.showNotification({
        message: 'Expense item updated successfully!'
      });
      this.listExpenses();
      modal.onNoClick();
    }).catch((err: any) => {
      this.notificationService.showNotification({
        message: err,
        type: 'danger'
      });
      this.listExpenses();
      modal.onNoClick();
    })
  }

  createExpenseItem(data: any, modal: any) {
    this.expenseService.createExpenseItem(data.expenseId, { name: data.name }).then((res: any) => {
      this.notificationService.showNotification({
        message: 'Expense item created successfully!'
      });
      this.listExpenses();
      modal.onNoClick();
    }).catch((err: any) => {
      this.notificationService.showNotification({
        message: err,
        type: 'danger'
      });
      this.listExpenses();
      modal.onNoClick();
    })
  }

}
