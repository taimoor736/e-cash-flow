import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateExpensesComponent } from './create.component';

describe('TableListComponent', () => {
  let component: CreateExpensesComponent;
  let fixture: ComponentFixture<CreateExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
