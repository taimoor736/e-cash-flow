import { Component, OnInit } from '@angular/core';
import { ExpenseService, NotificationService } from '../../shared/services/shared.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';


@Component({
  selector: 'app-create-expense',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateExpensesComponent implements OnInit {

  constructor(
    private expenseService: ExpenseService,
    private route: ActivatedRoute,
    private router: Router,
    private notificationService: NotificationService
  ) { }

  expenseForm = new FormGroup({
    name: new FormControl('', Validators.required),
  });
  expenseItems: any = [];
  ngOnInit() {
    this.addExpenseItem(1);
  }

  addExpenseItem(itemName: any) {
    if (!itemName)
      return;
    this.expenseItems.push({
      name: ''
    })
  }

  removeExpenseItem(index: any) {
    this.expenseItems.splice(index, 1);
  }

  creatExpense() {
    let expense = this.expenseForm.value;
    expense.expenseItems = this.expenseItems;
    this.expenseService.createExpense(this.expenseForm.value).then((res: any) => {
      this.notificationService.showNotification({
        message: 'Expense created successfully!'
      });
      this.router.navigate(['/list-expenses']);
    }).catch((err: any) => {
      this.notificationService.showNotification({
        message: err,
        type: 'danger'
      });
    })
  }

}
