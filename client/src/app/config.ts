import * as moment from 'moment';
export const Globals = {
    "pagination": {
        "limit": 25,
        "offset": 0,
        "itemsPerPage": 25
    },
    "dateFormat": function (date) {
        return moment(date).format('DD-MM-YYYY');
    }
}
