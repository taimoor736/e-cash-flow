import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModules } from '../material.module';
import { DashboardComponents, DashboardRoutesModule } from './dashboard.routing';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModules,
        DashboardRoutesModule,
        SharedModule
    ],
    declarations: [
        DashboardComponents
    ],
    providers: [
    ]
})
export class DashboardModule {}
