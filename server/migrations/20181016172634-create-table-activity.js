const moment = require('moment');

module.exports = {
  up: (queryInterface, Sequelize) => {
    const {
      INTEGER,
      DATE,
      NOW,
      DECIMAL,
      BOOLEAN,
    } = Sequelize;
    return queryInterface.createTable('activities', {
      id: {
        type: INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: 'id',
        onUpdate: 'cascade',
        onDelete: 'cascade',
      },
      employeeId: {
        type: INTEGER,
        field: 'employee_id',
        references: {
          model: 'users',
          key: 'id',
        },
      },
      projectId: {
        type: INTEGER,
        field: 'project_id',
        references: {
          model: 'projects',
          key: 'id',
        },
      },
      amount: {
        type: DECIMAL,
        field: 'amount',
        defaultValue: 0,
      },
      dateId: {
        type: INTEGER,
        field: 'date_id',
        references: {
          model: 'dates',
          key: 'id',
        },
      },
      isPaid: {
        type: BOOLEAN,
        defaultValue: false,
        field: 'is_paid',
      },
      createdAt: {
        type: DATE,
        defaultValue: NOW,
        field: 'created_at',
      },
      updatedAt: {
        type: DATE,
        defaultValue: NOW,
        field: 'updated_at',
      },
      deletedAt: {
        type: DATE,
        defaultValue: null,
        field: 'deleted_at',
      },
    });
  },

  down: queryInterface => queryInterface.dropTable('activities'),
};
