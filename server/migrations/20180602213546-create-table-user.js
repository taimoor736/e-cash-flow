module.exports = {
  up: (queryInterface, Sequelize) => {
    const {
      INTEGER,
      STRING,
      DATE,
      ENUM,
      NOW,
      DECIMAL,
    } = Sequelize;
    return queryInterface.createTable('users', {
      id: {
        type: INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: 'id',
        onUpdate: 'cascade',
        onDelete: 'cascade',
      },
      firstName: {
        type: STRING,
        field: 'first_name',
      },
      lastName: {
        type: STRING,
        field: 'last_name',
      },
      middleName: {
        type: STRING,
        field: 'middle_name',
      },
      email: {
        type: STRING,
        field: 'email',
        unique: true,
        allowNull: false,
        validate: {
          isEmail: {
            msg: 'Invalid email',
          },
        },
      },
      password: {
        type: STRING,
        field: 'password',
        allowNull: false,
      },
      addresses: {
        // NOTE: In future We might need to take this in a separate table. But for now let it be.
        type: STRING,
        field: 'addresses',
      },
      roles: {
        type: ENUM,
        field: 'roles',
        allowNull: false,
        values: ['admin', 'employee'],
      },
      balance: {
        type: DECIMAL,
        field: 'balance',
        defaultValue: 0,
      },
      rate: {
        type: INTEGER,
        field: 'rate',
        defaultValue: 0,
      },
      createdAt: {
        type: DATE,
        defaultValue: NOW,
        field: 'created_at',
      },
      updatedAt: {
        type: DATE,
        defaultValue: NOW,
        field: 'updated_at',
      },
      deletedAt: {
        type: DATE,
        defaultValue: null,
        field: 'deleted_at',
      },
    });
  },

  down: queryInterface => queryInterface.dropTable('user'),
};
