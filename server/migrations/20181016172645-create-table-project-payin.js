const moment = require('moment');

module.exports = {
  up: (queryInterface, Sequelize) => {
    const {
      INTEGER,
      DATE,
      NOW,
      DECIMAL,
    } = Sequelize;
    return queryInterface.createTable('project_payins', {
      id: {
        type: INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: 'id',
        onUpdate: 'cascade',
        onDelete: 'cascade',
      },
      projectId: {
        type: INTEGER,
        field: 'project_id',
        references: {
          model: 'projects',
          key: 'id',
        },
      },
      amount: {
        type: DECIMAL,
        field: 'amount',
        defaultValue: 0,
      },
      dateId: {
        type: INTEGER,
        field: 'date_id',
        references: {
          model: 'dates',
          key: 'id',
        },
      },
      createdAt: {
        type: DATE,
        defaultValue: NOW,
        field: 'created_at',
      },
      updatedAt: {
        type: DATE,
        defaultValue: NOW,
        field: 'updated_at',
      },
      deletedAt: {
        type: DATE,
        defaultValue: null,
        field: 'deleted_at',
      },
    });
  },

  down: queryInterface => queryInterface.dropTable('project_payins'),
};
