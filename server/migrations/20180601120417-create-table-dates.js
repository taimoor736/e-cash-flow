const moment = require('moment');

module.exports = {
  up: (queryInterface, Sequelize) => {
    const {
      INTEGER,
      DATEONLY,
      DATE,
      NOW,
    } = Sequelize;
    return queryInterface.createTable('dates', {
      id: {
        type: INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: 'id',
        onUpdate: 'cascade',
        onDelete: 'cascade',
      },
      date: {
        type: DATEONLY,
        field: 'date',
        defaultValue: moment().format('YYYY-MM-DD'),
      },
      createdAt: {
        type: DATE,
        defaultValue: NOW,
        field: 'created_at',
      },
      updatedAt: {
        type: DATE,
        defaultValue: NOW,
        field: 'updated_at',
      },
      deletedAt: {
        type: DATE,
        defaultValue: null,
        field: 'deleted_at',
      },
    });
  },

  down: queryInterface => queryInterface.dropTable('dates'),
};
