module.exports = {
  up: (queryInterface, Sequelize) => {
    const {
      STRING,
      INTEGER,
      DATE,
      DATEONLY,
      NOW,
      TEXT,
    } = Sequelize;
    return queryInterface.createTable('projects', {
      id: {
        type: INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: 'id',
        onUpdate: 'cascade',
        onDelete: 'cascade',
      },
      name: {
        type: STRING,
        field: 'name',
        defaultValue: 'Unnamed Project',
      },
      startDate: {
        type: DATEONLY,
        field: 'start_date',
        defaultValue: NOW,
      },
      endDate: {
        type: DATEONLY,
        field: 'end_date',
        defaultValue: null,
      },
      amount: {
        type: INTEGER,
        field: 'amount',
      },
      notes: {
        type: TEXT,
        field: 'notes',
        defaultValue: '',
      },
      createdAt: {
        type: DATE,
        defaultValue: NOW,
        field: 'created_at',
      },
      updatedAt: {
        type: DATE,
        defaultValue: NOW,
        field: 'updated_at',
      },
      deletedAt: {
        type: DATE,
        defaultValue: null,
        field: 'deleted_at',
      },
    });
  },
  down: queryInterface => queryInterface.dropTable('project'),
};
