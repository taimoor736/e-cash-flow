const app = require('./index');
const http = require('http');
/*
 * Create and start HTTP server.
 */

const server = http.createServer(app);
server.listen(process.env.PORT || 8001);
server.on('listening', () => {
  global.logger.info('Server listening on http://localhost:%d', server.address().port);
});
