module.exports = {
  v1: {
    admin: {
      employee: {
        body: {
          email: 'string',
          firstName: 'string',
          lastName: 'string',
        },
        query: {},
        method: 'post',
      },
    },
  },
};
