const {
  db,
  asyncMiddleware,
  middlewares,
} = global;

module.exports = (router) => {
  router.get('/', asyncMiddleware(async (req, res) => {
    const query = {
      where: {},
      order: [['createdAt', 'desc']],
    };
    if (req.query.name) {
      query.where.name = {
        [db.Sequelize.Op.like]: `${req.query.name}%`,
      };
    }
    const projects = await db.Projects.findAll(query);
    return res.status(200).json(projects);
  }));
  router.post('/', asyncMiddleware(async (req, res) => {
    const newProject = req.body;
    delete newProject.id;
    const savedProject = await (new db.Projects(newProject)).save();
    return res.status(200).json(savedProject);
  }));
  router.param('projectId', asyncMiddleware(async (req, res, next, projectId) => {
    if (!projectId) {
      next({
        message: 'Project id is required in params.',
      });
      return;
    }
    const project = await db.Projects.findById(projectId);
    if (!project) {
      next({
        message: 'Invalid project id.',
      });
      return;
    }
    req.project = project;
    next();
  }));
  router.get('/:projectId', asyncMiddleware(async (req, res) => {
    const employees = await req.project.getUsers({
      include: [{
        model: db.UserProjectPayouts,
        where: {
          projectId: req.project.id,
        },
        as: 'pays',
        required: false,
      }],
    });
    const expenses = await req.project.getExpenses({
      include: [
        {
          model: db.ExpenseItems,
          as: 'expenseDetail',
          include: [{
            model: db.Expenses,
            as: 'parent',
          }],
        },
      ],
    });
    res.json({
      ...req.project.toJSON(),
      employees,
      expenses,
    });
  }));
  router.put('/:projectId', asyncMiddleware(async (req, res) => {
    delete req.body.id;
    const updatedProject = await req.project.update(req.body);
    return res.status(200).json(updatedProject);
  }));
  router.delete('/:projectId', asyncMiddleware(async (req, res) => {
    await req.project.destroy();
    return res.status(200).json({
      message: 'Deleted project successfully',
    });
  }));
  router.post('/:projectId/expenses', asyncMiddleware(middlewares.findCreateDate()), asyncMiddleware(async (req, res) => {
    const { name, amount } = req.body;
    const newExpense = await db.ProjectExpenses.create({
      name,
      amount,
      projectId: req.project.id,
      dateId: req.date.id,
    });
    res.status(200).send(newExpense);
  }));
  router.post('/:projectId/payins', asyncMiddleware(middlewares.findCreateDate()), asyncMiddleware(async (req, res) => {
    const { amount } = req.body;
    const newPayIn = await db.ProjectPayins.create({
      amount,
      projectId: req.project.id,
      dateId: req.date.id,
    });
    res.status(200).send(newPayIn);
  }));
};
