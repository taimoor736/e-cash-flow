const {
  db,
  asyncMiddleware,
  middlewares,
} = global;

module.exports = (router) => {
  async function getEmployee(req, res, next) {
    const id = req.body.employeeId || req.params.employeeId || req.query.employeeId;
    const employee = await db.Users.findOne({
      where: {
        id,
        roles: 'employee',
      },
    });
    if (!employee) {
      next({
        message: 'Invalid Employee Id',
        status: 400,
      });
      return;
    }
    req.employee = employee;
    next();
  }
  router.post('/', asyncMiddleware(middlewares.findCreateDate()), asyncMiddleware(getEmployee), asyncMiddleware(async (req, res) => {
    const { amount, projectId } = req.body;
    const activity = await req.employee.logActivity({
      amount,
      projectId,
      dateId: req.date.id,
    });
    res.status(200).send(activity);
  }));
  router.get('/', asyncMiddleware(async (req, res) => {
    const {
      projectId,
      employeeId,
      ...rest
    } = req.query;
    const filters = {
      ...rest,
      limit: req.limit,
      offset: req.offset,
    };
    const where = {};
    if (projectId) {
      where.projectId = Number(projectId);
    }
    if (employeeId) {
      where.employeeId = Number(employeeId);
    }
    const include = [
      {
        where,
        model: db.Activities,
        as: 'activities',
        include: [
          {
            model: db.Users,
            as: 'employee',
          },
          {
            model: db.Projects,
            as: 'project',
            required: false,
          },
        ],
      },
    ];
    const activities = await db.Dates.getResultsOf({
      filters,
      include,
    });
    res.status(200).send(activities).end();
  }));
};
