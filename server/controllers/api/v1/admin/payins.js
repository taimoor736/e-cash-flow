const {
  db,
  asyncMiddleware,
} = global;

module.exports = (router) => {
  router.get('/', asyncMiddleware(async (req, res) => {
    const {
      projectId,
      ...rest
    } = req.query;
    const filters = {
      ...rest,
      limit: req.limit,
      offset: req.offset,
    };
    const where = {};
    if (projectId) {
      where.projectId = Number(projectId);
    }
    const include = [
      {
        where,
        model: db.ProjectPayins,
        as: 'payins',
        required: true,
      },
    ];
    const payins = await db.Dates.getResultsOf({
      filters,
      include,
    });
    res.status(200).send(payins).end();
  }));
};
