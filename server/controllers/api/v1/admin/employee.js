const {
  db,
  asyncMiddleware,
  commonFunctions,
} = global;

module.exports = (router) => {
  router.get('/', asyncMiddleware(async (req, res) => {
    const query = {
      where: {
        roles: 'employee',
      },
    };
    if (req.query.search) {
      const likeObject = {
        [db.Sequelize.Op.like]: `${req.query.search}%`,
      };
      query.where[db.Sequelize.Op.or] = [
        {
          firstName: likeObject,
        },
        {
          lastName: likeObject,
        },
        {
          middleName: likeObject,
        },
        {
          email: likeObject,
        },
        {
          addresses: likeObject,
        },
      ];
    }
    const employees = await db.Users.findAll(query);
    return res.status(200).json(employees);
  }));
  router.post('/', asyncMiddleware(async (req, res) => {
    const password = commonFunctions.generateRandomPassword();
    const newEmployee = new db.Users({
      ...req.body,
      password,
      roles: 'employee',
    });
    const savedEmployee = await newEmployee.save();
    // TODO: NEED TO IMPLEMENT THE EMAIL Password to employee here;
    return res.status(200).json(savedEmployee);
  }));
  router.param('employeeId', asyncMiddleware(async (req, res, next, employeeId) => {
    const employee = await db.Users.findOne({
      where: {
        id: employeeId,
        roles: 'employee',
      },
    });
    if (!employee) {
      next({
        status: 400,
        message: 'Invalid employee id.',
      });
      return;
    }
    req.employee = employee;
    next();
  }));
  router.get('/:employeeId', asyncMiddleware(async (req, res) => {
    res.json(req.employee);
  }));
  router.put('/:employeeId', asyncMiddleware(async (req, res) => {
    delete req.body.roles;
    const updatedEmployee = await req.employee.update(req.body);
    return res.status(200).json(updatedEmployee);
  }));
  router.delete('/:employeeId', asyncMiddleware(async (req, res) => {
    await req.employee.destroy();
    return res.status(200).json({ message: 'Deleted employee successfully.' });
  }));
};
