const {
  db,
  asyncMiddleware,
} = global;

module.exports = (router) => {
  router.get('/', asyncMiddleware(async (req, res) => {
    const {
      projectId,
      name,
      ...rest
    } = req.query;
    const filters = {
      limit: req.limit,
      offset: req.offset,
      ...rest,
    };
    const where = {};
    if (projectId) {
      where.projectId = Number(projectId);
    }
    if (name) {
      where.name = {
        [db.Sequelize.Op.like]: `${name}%`,
      };
    }
    const include = [
      {
        where,
        model: db.ProjectExpenses,
        as: 'expenses',
        required: true,
      },
    ];
    const expenses = await db.Dates.getResultsOf({
      filters,
      include,
    });
    res.status(200).send(expenses).end();
  }));
};
