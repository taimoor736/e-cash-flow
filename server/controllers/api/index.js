const {
  asyncMiddleware,
  commonFunctions,
  db,
} = global;

module.exports = (router) => {
  router.post('/login', asyncMiddleware(async (req, res, next) => {
    const { email, password } = req.body;
    const user = await db.Users.findOne({
      where: {
        email,
        password: commonFunctions.hashPassword(password),
      },
    });
    if (!user) {
      next({ status: 401, message: 'Invalid Credentials' });
      return;
    }
    const payload = { userId: user.id };
    res.status(200).json({
      token: commonFunctions.generateToken({ payload }),
      user,
    });
  }));

  router.post('/signup', asyncMiddleware(async (req, res) => {
    const newUser = new db.Users({
      ...req.body,
      roles: 'employee',
    });
    const savedUser = await newUser.save();
    const payload = { userId: savedUser.id };
    res.status(200).json({
      token: commonFunctions.generateToken({ payload }),
    });
  }));

  router.get('/me', asyncMiddleware(async (req, res) => {
    res.status(200).send({
      user: req.user,
    });
  }));
};
