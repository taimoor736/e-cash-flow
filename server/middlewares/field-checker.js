/**
 * Created by Sajjad Ali on 6/12/2017.
 * Updated by Sajjad Ali on 14/2/2018.
 */

const requiredKeys = require('../config/keys-required');
const _ = require('lodash');


module.exports = function fieldChecker() {
  return (req, res, next) => {
    const targetKeys = findTargetKeysAgainstRoute(req.url);
    global.logger.debug(JSON.stringify(targetKeys || {}));
    if (targetKeys && targetKeys.method === req.method.toLowerCase()) {
      const error = requiredFieldChecker(targetKeys, parseRequestingObj(req));
      if (error) {
        next({
          status: 400,
          message: error,
        });
        return;
      }
    }
    next();
  };
};


var findTargetKeysAgainstRoute = function (url) {
  let splitRoute = _.split((_.split(url, '/api/'))[1], '/');
  let allFields = _.clone(requiredKeys);
  for (let i = 0; i < splitRoute.length; i++) {
    let value = splitRoute[i];
    if (typeof allFields[value] === 'object') {
      allFields = allFields[value]
    } else {
      if (allFields[splitRoute.slice(i).join('/')]) {
        allFields = allFields[splitRoute.slice(i).join('/')];
      } else {
        let keys = _.keys(allFields);

        let remainingRoute;
        for (let index = 0; index < keys.length; index++) {
          remainingRoute = splitRoute.slice(i);
          let currentKeys = _.split(keys[index],'/');

          if (currentKeys.length === remainingRoute.length) {
            let flag = false;
            currentKeys.forEach((value, index) => {
              if (~value.indexOf(':') && currentKeys[currentKeys.length - 1] === remainingRoute[remainingRoute.length - 1]) {
                remainingRoute[index] = value;
                flag=true;
                global.logger.debug(remainingRoute,'=========>  ');
              }
            });
           if (flag) {
             break;
           }
          }
        }
        allFields = allFields[remainingRoute.join('/')];

        global.logger.debug(remainingRoute.join('/'),'=>>>>>>',allFields);
        break;
      }
    }
  }

  return allFields;
}

var parseRequestingObj = function (req) {
  return {
    body: emptyOrNullKeyFilter(req.body),
    query: emptyOrNullKeyFilter(req.query),
    // query: parseQuery(emptyOrNullKeyFilter(req.query))
  };
};

var emptyOrNullKeyFilter = function (body) {
  return _.omitBy(body, (value, key) => body[key] == '');
};
var requiredFieldChecker = function (requiredObject, requestingObject) {
  let error = '';
  requestingObject = makeRequestingObjectEqualInLengthToRequiredObject(requiredObject, requestingObject);

  error = findErrorInQueryOrBody(requiredObject.body, requestingObject.body);
  if (error) { return `${error} in body`; }
  error = getDiffernceInValueType(requiredObject.body, requestingObject.body);
  if (error) { return `${error} in body`; }
  error = findErrorInQueryOrBody(requiredObject.query, requestingObject.query);
  if (error) { return `${error} in query`; }
  return error;
};

var makeRequestingObjectEqualInLengthToRequiredObject = function (requiredObject, requestingObject) {
  requestingObject.body = _.omit(requestingObject.body, _.difference(_.keys(requestingObject.body), _.keys(requiredObject.body)));
  requestingObject.query = _.omit(requestingObject.query, _.difference(_.keys(requestingObject.query), _.keys(requiredObject.query)));
  return requestingObject;
};


var findErrorInQueryOrBody = function (obj1, obj2) {
  const arrayOfMissingFields = getDiffernceInKeys(obj1, obj2);
  return errorMaker(arrayOfMissingFields);
};


var getDiffernceInKeys = function (obj1, obj2) {
  return _.difference(_.keys(obj1), _.keys(obj2));
};


var errorMaker = function (array) {
  return array.length ? array.join(',') + getErrorSubString(array) : '';
};


var getErrorSubString = function (array) {
  return array.length > 1 ? ' are required' : ' is required';
};

var getDiffernceInValueType = function (obj1, obj2) {
  obj1 = sortObject(obj1);
  obj2 = sortObject(obj2);

  const obj1keys = _.keys(obj1);
  const obj1Values = _.values(obj1);
  const obj2Values = _.values(obj2);


  return _.chain(obj2Values).map(obj => (_.isArray(obj) ? 'array' : typeof obj)).map((inCommingType, key) => {
    const fieldName = obj1keys[key];
    const typeShouldBe = obj1Values[key];
    const valueOfIcommingField = obj2Values[key];
    const objectsTypeFlag = !!~_.indexOf(['object', 'array'], inCommingType);
    if (typeShouldBe != inCommingType || objectsTypeFlag) {
      if (!objectsTypeFlag) {
        return `Type of ${fieldName} should be ${typeShouldBe}`;
      } else if (_.isArray(typeShouldBe)) {
        if (_.isEmpty(typeShouldBe)) {
          if (_.isEmpty(valueOfIcommingField)) {
            return `${fieldName} is required in body and type of ${fieldName} is array which should not be empty`;
          }
          return '';
        }
        let internalError;
        for (let index = 0; index < valueOfIcommingField.length; index++) {
          internalError = findErrorInQueryOrBody(typeShouldBe[0], emptyOrNullKeyFilter(valueOfIcommingField[index]));
          if (internalError) {
            internalError = `${internalError} in field of ${fieldName} at ${++index} object of array`;
            break;
          } else {
            internalError = getDiffernceInValueType(typeShouldBe[0], emptyOrNullKeyFilter(valueOfIcommingField[index]));
            if (internalError) {
              internalError = `${internalError} in field of ${fieldName} at ${++index} object of array`;
              break;
            }
          }
        }
        return internalError;
      } else if (typeShouldBe) {
        let internalError = '';
        internalError = findErrorInQueryOrBody(typeShouldBe, emptyOrNullKeyFilter(valueOfIcommingField));
        if (internalError) {
          return `${internalError} in field of ${fieldName}`;
        }
        internalError = getDiffernceInValueType(typeShouldBe, emptyOrNullKeyFilter(valueOfIcommingField));
        if (internalError) {
          return internalError;
        }
      }
    }
  }).compact()
    .take(1)
    .value()[0];
};

var sortObject = function (obj) {
  return _.fromPairs(_.map(_.sortBy(_.keys(obj)), key => [key, obj[key]]));
};
