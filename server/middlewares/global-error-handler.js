/*
* Overriding this rule because Kraken.js requires the function signature to have next even if not
* using it.
* */
/*
eslint no-unused-vars: 0
*/
module.exports = () => (error, req, res, next) => {
  global.logger.error(error);
  res.status(200).json(error).end();
};
