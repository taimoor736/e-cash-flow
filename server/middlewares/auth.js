const { verify } = require('jsonwebtoken');
const { promisify } = require('bluebird');

const jwtVerify = promisify(verify);
const { asyncMiddleware, db } = global;

module.exports = () => asyncMiddleware(async (req, res, next) => {
  const { authorization } = req.headers;
  if (!authorization) {
    next({
      status: 401,
      message: 'Missing Authorization Error.',
    });
    return;
  }
  const token = authorization.split(' ')[1];
  if (!token) {
    next({
      status: 401,
      message: 'Missing Token',
    });
  }
  const { userId } = await jwtVerify(token, global.kraken.get('app:constants:jwtSecret'));
  const user = await db.Users.findById(userId);
  if (!user) {
    next({
      status: 401,
      message: 'Invalid Token',
    });
    return;
  }
  req.user = user;
  next();
});
