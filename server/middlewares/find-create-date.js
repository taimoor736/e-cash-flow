module.exports = () => async function _getDate(req, res, next) {
  const { date } = req.body;
  const dateObject = await global.db.Dates.findCreate(date);
  req.date = dateObject;
  next();
};
