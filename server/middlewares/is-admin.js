function isAdminUser({ user }) {
  return (user.roles === 'admin');
}
module.exports = () => (req, res, next) => {
  if (!isAdminUser({ user: req.user })) {
    next({
      status: 401,
      message: 'Access denied.',
    });
    return;
  }
  next();
};
