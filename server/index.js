const express = require('express');
const kraken = require('kraken-js');
const path = require('path');
const {
  logger,
  commonFunctions,
} = require('./lib');

global.asyncMiddleware = fn => (req, res, next, ...args) => {
  Promise
    .resolve(fn(req, res, next, ...args))
    .catch((err) => {
      global.logger.debug(err);
      // TODO: Error Parsing Here..ss
      next(err);
    });
};

global.logger = logger;
global.commonFunctions = commonFunctions;
global.db = require('./models');
global.middlewares = require('./middlewares');
/*
 * Create and configure application. Also exports application instance for use by tests.
 * See https://github.com/krakenjs/kraken-js#options for additional configuration options.
 */

const options = {
  onconfig(config, next) {
    /*
         * Add any additional config setup or overrides here. `config` is an initialized
         * `confit` (https://github.com/krakenjs/confit/) configuration object.
         */
    next(null, config);
  },
};

const { globalErrorHandler } = global.middlewares;

const app = express();
app.use(express.static(path.resolve(path.join(__dirname, '../public'))));
app.use(kraken(options));
app.on('start', () => {
  global.kraken = app.kraken;
  global.logger.info('Application ready to serve requests.');
  global.logger.info('Environment: %s', app.kraken.get('env:env'));
});
app.use(globalErrorHandler());
module.exports = app;
