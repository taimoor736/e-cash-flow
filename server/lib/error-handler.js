const { has } = require('lodash');

const handlers = {
  SequelizeDatabaseError(error) {},
  SequelizeUniqueConstraintError(error) {},
  SequelizeValidationError(error) {},
  unKnownError() {},
};
function errorHandler({ error = {} }) {
  if (has(handlers, error.name)) {
    return handlers[error.name](error);
  }
  return handlers.unKnownError();
}

module.exports = errorHandler;
module.exports.errorHandler = errorHandler;
