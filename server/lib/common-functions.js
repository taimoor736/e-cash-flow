const { sign } = require('jsonwebtoken');
const md5 = require('md5');

function hashPassword(password) {
  return md5(password);
}

function generateToken({ payload = {} } = {}) {
  const secret = global.kraken.get('app:constants:jwtSecret');
  const options = global.kraken.get('app:tokenConfig');
  return sign(payload, secret, options);
}

function generateRandomPassword({ length = 10 } = {}) {
  return Math.random().toString(36).slice(length);
}

module.exports = {
  hashPassword,
  generateToken,
  generateRandomPassword,
};
