/*
  eslint global-require: 0
*/
const { readdirSync } = require('fs');
const { camelCase } = require('lodash');
const path = require('path');

const basename = path.basename(__filename);
const dir = {};
readdirSync(__dirname)
  .filter(file => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js'))
  .forEach((file) => {
    const normalizedName = camelCase(file.replace('.js', ''));
    dir[normalizedName] = require(path.resolve(path.join(__dirname, file)));
  });
module.exports = dir;
