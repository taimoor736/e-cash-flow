const bunyan = require('bunyan');

module.exports = bunyan.createLogger({
  name: 'budgetManagement',
  level: 10,
});
