const { startCase } = require('lodash');

const { hashPassword } = global.commonFunctions;
module.exports = (sequelize, Sequelize) => {
  const {
    INTEGER,
    STRING,
    ENUM,
    DATE,
    NOW,
    DECIMAL,
  } = Sequelize;
  const Users = sequelize.define('users', {
    id: {
      type: INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'id',
      onUpdate: 'cascade',
      onDelete: 'cascade',
    },
    firstName: {
      type: STRING,
      field: 'first_name',
      set: function _setFirstName(val) {
        return this.setDataValue('firstName', startCase(val));
      },
    },
    lastName: {
      type: STRING,
      field: 'last_name',
      set: function _setLastName(val) {
        return this.setDataValue('lastName', startCase(val));
      },
    },
    middleName: {
      type: STRING,
      field: 'middle_name',
      set: function _setMiddleName(val) {
        return this.setDataValue('middleName', startCase(val));
      },
    },
    email: {
      type: STRING,
      field: 'email',
      unique: {
        args: true,
        msg: 'Email already exists',
      },
      allowNull: false,
      validate: {
        isEmail: {
          msg: 'Invalid email',
        },
      },
    },
    password: {
      type: STRING,
      field: 'password',
      allowNull: false,
      set: function _setPassword(val) {
        return this.setDataValue('password', hashPassword(val));
      },
    },
    addresses: {
      // NOTE: In future We might need to take this in a separate table. But for now let it be.
      type: STRING,
      field: 'addresses',
    },
    roles: {
      type: ENUM,
      field: 'roles',
      allowNull: false,
      values: ['admin', 'employee'],
      set: function _setRoles(val) {
        return this.setDataValue('roles', val.toLowerCase());
      },
    },
    balance: {
      type: DECIMAL,
      field: 'balance',
      defaultValue: 0,
    },
    rate: {
      type: INTEGER,
      field: 'rate',
      defaultValue: 0,
    },
    createdAt: {
      type: DATE,
      defaultValue: NOW,
      field: 'created_at',
    },
    updatedAt: {
      type: DATE,
      defaultValue: NOW,
      field: 'updated_at',
    },
    deletedAt: {
      type: DATE,
      defaultValue: null,
      field: 'deleted_at',
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    deletedAt: 'deletedAt',
  });
  Users.associate = (models) => {
    Users.hasMany(models.Activities, {
      foreignKey: 'employeeId',
      as: 'activities',
    });
  };
  Users.prototype.toJSON = function _toJSON() {
    const values = Object.assign({}, this.get());
    delete values.password;
    return values;
  };
  Users.prototype.logActivity = async function _logActivity({ amount = 0, dateId, ...rest } = {}) {
    const activities = await this.getActivities({
      where: {
        dateId,
      },
      limit: 1,
    });
    if (activities.length) return activities[0];
    const newActivity = {
      dateId,
      ...rest,
      amount: amount || this.rate,
      employeeId: this.id,
      isPaid: !!amount,
    };
    const savedActivity = await global.db.Activities.create(newActivity);
    await this.update({
      balance: ((Number(this.balance) + Number(this.rate)) - amount),
    });
    return savedActivity;
  };
  return Users;
};
