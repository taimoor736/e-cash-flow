module.exports = (sequelize, Sequelize) => {
  const {
    INTEGER,
    DATE,
    NOW,
  } = Sequelize;
  const Quotations = sequelize.define('quotations', {
    id: {
      type: INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'id',
      onUpdate: 'cascade',
      onDelete: 'cascade',
    },
    createdAt: {
      type: DATE,
      defaultValue: NOW,
      field: 'created_at',
    },
    updatedAt: {
      type: DATE,
      defaultValue: NOW,
      field: 'updated_at',
    },
    deletedAt: {
      type: DATE,
      defaultValue: null,
      field: 'deleted_at',
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    deletedAt: 'deletedAt',
  });
  return Quotations;
};
