const moment = require('moment');

module.exports = (sequelize, Sequelize) => {
  const {
    INTEGER,
    DATE,
    NOW,
    DECIMAL,
  } = Sequelize;
  const ProjectPayins = sequelize.define('project_payins', {
    id: {
      type: INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'id',
      onUpdate: 'cascade',
      onDelete: 'cascade',
    },
    projectId: {
      type: INTEGER,
      field: 'project_id',
      references: {
        model: 'projects',
        key: 'id',
      },
    },
    amount: {
      type: DECIMAL,
      field: 'amount',
      defaultValue: 0,
    },
    dateId: {
      type: INTEGER,
      field: 'date_id',
      references: {
        model: 'dates',
        field: 'id',
      },
    },
    createdAt: {
      type: DATE,
      defaultValue: NOW,
      field: 'created_at',
    },
    updatedAt: {
      type: DATE,
      defaultValue: NOW,
      field: 'updated_at',
    },
    deletedAt: {
      type: DATE,
      defaultValue: null,
      field: 'deleted_at',
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    deletedAt: 'deletedAt',
  });
  ProjectPayins.associate = (models) => {
    ProjectPayins.belongsTo(models.Projects, {
      foreignKey: 'projectId',
      as: 'project',
    });
    ProjectPayins.belongsTo(models.Dates, {
      foreignKey: 'dateId',
      as: 'date',
    });
  };
  return ProjectPayins;
};
