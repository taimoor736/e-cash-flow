const { startCase } = require('lodash');
const moment = require('moment');

module.exports = (sequelize, Sequelize) => {
  const {
    STRING,
    INTEGER,
    DATEONLY,
    TEXT,
    DATE,
    NOW,
  } = Sequelize;
  const Projects = sequelize.define('projects', {
    id: {
      type: INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'id',
      onUpdate: 'cascade',
      onDelete: 'cascade',
    },
    name: {
      type: STRING,
      field: 'name',
      defaultValue: 'Unnamed Project',
      set: function _setProjectName(val) {
        return this.setDataValue('name', startCase(val));
      },
    },
    startDate: {
      type: DATEONLY,
      field: 'start_date',
      defaultValue: moment().format('YYYY-MM-DD'),
    },
    endDate: {
      type: DATEONLY,
      field: 'end_date',
      defaultValue: null,
    },
    amount: {
      type: INTEGER,
      field: 'amount',
    },
    notes: {
      type: TEXT,
      field: 'notes',
      defaultValue: '',
    },
    createdAt: {
      type: DATE,
      defaultValue: NOW,
      field: 'created_at',
    },
    updatedAt: {
      type: DATE,
      defaultValue: NOW,
      field: 'updated_at',
    },
    deletedAt: {
      type: DATE,
      defaultValue: null,
      field: 'deleted_at',
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    deletedAt: 'deletedAt',
  });
  Projects.associate = (models) => {
    Projects.hasMany(models.ProjectPayins, {
      foreignKey: 'projectId',
      as: 'payins',
    });
    Projects.hasMany(models.ProjectExpenses, {
      foreignKey: 'projectId',
      as: 'expenses',
    });
    Projects.hasMany(models.Activities, {
      foreignKey: 'projectId',
      as: 'activities',
    });
  };
  return Projects;
};
