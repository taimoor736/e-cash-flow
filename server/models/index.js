// Turning of the global require intentionally.
/*
  eslint global-require: 0
*/
const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const { startCase } = require('lodash');

const basename = path.basename(__filename);

const config = require('../config/database.js');

const db = {};
const {
  database,
  username,
  password,
} = config;
const sequelize = new Sequelize(database, username, password, config);

sequelize
  .authenticate()
  .then(() => {
    global.logger.info('Database connection has been established successfully.');
  })
  .done();

function getNormalizedNameOfModel(modelName) {
  return startCase(modelName).replace(/\s/g, '');
}
fs
  .readdirSync(__dirname)
  .filter(file => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js'))
  .forEach((file) => {
    const model = sequelize.import(path.join(__dirname, file));
    const normalizedName = getNormalizedNameOfModel(model.name);
    db[normalizedName] = model;
  });

Object.keys(db).forEach((modelName) => {
  const normalizedName = getNormalizedNameOfModel(modelName);
  if (db[normalizedName].associate) {
    db[normalizedName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
