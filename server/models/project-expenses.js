module.exports = (sequelize, Sequelize) => {
  const {
    INTEGER,
    DATE,
    NOW,
    DECIMAL,
    STRING,
  } = Sequelize;
  const ProjectExpenses = sequelize.define('project_expenses', {
    id: {
      type: INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'id',
      onUpdate: 'cascade',
      onDelete: 'cascade',
    },
    name: {
      type: STRING,
      field: 'name',
      defaultValue: 'Unnamed Expense',
    },
    projectId: {
      type: INTEGER,
      field: 'project_id',
      references: {
        model: 'projects',
        key: 'id',
      },
    },
    amount: {
      type: DECIMAL,
      field: 'amount',
      defaultValue: 0,
    },
    dateId: {
      type: INTEGER,
      field: 'date_id',
      references: {
        model: 'dates',
        field: 'id',
      },
    },
    createdAt: {
      type: DATE,
      defaultValue: NOW,
      field: 'created_at',
    },
    updatedAt: {
      type: DATE,
      defaultValue: NOW,
      field: 'updated_at',
    },
    deletedAt: {
      type: DATE,
      defaultValue: null,
      field: 'deleted_at',
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    deletedAt: 'deletedAt',
  });
  ProjectExpenses.associate = (models) => {
    ProjectExpenses.belongsTo(models.Projects, {
      foreignKey: 'projectId',
      as: 'project',
    });
    ProjectExpenses.belongsTo(models.Dates, {
      foreignKey: 'dateId',
      as: 'date',
    });
  };
  return ProjectExpenses;
};
