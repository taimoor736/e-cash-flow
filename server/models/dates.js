const moment = require('moment');
const { upperCase } = require('lodash');

module.exports = (sequelize, Sequelize) => {
  const {
    INTEGER,
    DATE,
    NOW,
    DATEONLY,
    VIRTUAL,
  } = Sequelize;
  const Dates = sequelize.define('dates', {
    id: {
      type: INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'id',
      onUpdate: 'cascade',
      onDelete: 'cascade',
    },
    date: {
      type: DATEONLY,
      field: 'date',
      defaultValue: moment().format('YYYY-MM-DD'),
    },
    year: {
      type: VIRTUAL,
      get: function _getActivity() {
        return moment(this.date).format('YYYY');
      },
    },
    month: {
      type: VIRTUAL,
      get: function _getMonth() {
        return moment(this.date).format('YYYY-MM');
      },
    },
    week: {
      type: VIRTUAL,
      get: function _getWeek() {
        return moment(this.date).weeks();
      },
    },
    createdAt: {
      type: DATE,
      defaultValue: NOW,
      field: 'created_at',
    },
    updatedAt: {
      type: DATE,
      defaultValue: NOW,
      field: 'updated_at',
    },
    deletedAt: {
      type: DATE,
      defaultValue: null,
      field: 'deleted_at',
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    deletedAt: 'deletedAt',
  });
  Dates.findCreate = function _findCreate(date) {
    return new Promise((resolve) => {
      Dates.findOrCreate({
        where: {
          date,
        },
        defaults: {
          date,
        },
      }).spread(newDate => resolve(newDate));
    });
  };
  Dates.getResultsOf = async function _getResultsOf({ include, filters }) {
    const {
      from,
      to,
      orderBy,
      limit,
      offset,
    } = filters;
    const query = {
      limit,
      offset,
      include,
      where: {},
      order: [['date', upperCase(orderBy) || 'DESC']],

    };

    if (from && to) {
      query.where.date = {
        [global.db.Sequelize.Op.between]: [from, to],
      };
    }
    return Dates.findAll(query);
  };
  Dates.associate = (models) => {
    Dates.hasMany(models.Activities, {
      foreignKey: 'dateId',
      as: 'activities',
    });
    Dates.hasMany(models.ProjectExpenses, {
      foreignKey: 'dateId',
      as: 'expenses',
    });
    Dates.hasMany(models.ProjectPayins, {
      foreignKey: 'dateId',
      as: 'payins',
    });
  };
  return Dates;
};
