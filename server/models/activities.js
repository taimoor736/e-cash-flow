const moment = require('moment');

module.exports = (sequelize, Sequelize) => {
  const {
    INTEGER,
    DATE,
    NOW,
    DECIMAL,
    BOOLEAN,
  } = Sequelize;
  const Activities = sequelize.define('activities', {
    id: {
      type: INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'id',
      onUpdate: 'cascade',
      onDelete: 'cascade',
    },
    employeeId: {
      type: INTEGER,
      field: 'employee_id',
      references: {
        model: 'users',
        key: 'id',
      },
    },
    projectId: {
      type: INTEGER,
      field: 'project_id',
      references: {
        model: 'projects',
        key: 'id',
      },
    },
    amount: {
      type: DECIMAL,
      field: 'amount',
      defaultValue: 0,
    },
    dateId: {
      type: INTEGER,
      field: 'date_id',
      references: {
        model: 'dates',
        field: 'id',
      },
    },
    isPaid: {
      type: BOOLEAN,
      defaultValue: false,
      field: 'is_paid',
    },
    createdAt: {
      type: DATE,
      defaultValue: NOW,
      field: 'created_at',
    },
    updatedAt: {
      type: DATE,
      defaultValue: NOW,
      field: 'updated_at',
    },
    deletedAt: {
      type: DATE,
      defaultValue: null,
      field: 'deleted_at',
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    deletedAt: 'deletedAt',
  });
  Activities.associate = (models) => {
    Activities.belongsTo(models.Projects, {
      foreignKey: 'projectId',
      as: 'project',
    });
    Activities.belongsTo(models.Users, {
      foreignKey: 'employeeId',
      as: 'employee',
    });
    Activities.belongsTo(models.Dates, {
      foreignKey: 'dateId',
      as: 'date',
    });
  };
  return Activities;
};
